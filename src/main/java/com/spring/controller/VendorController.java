package com.spring.controller;

import com.spring.entity.Vendor;
import com.spring.exception.RecordNotFoundException;
import com.spring.service.VendorService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Description;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/vendors")
@Api(value = "vendormanagement", description = "Operations pertaining to vendors")
public class VendorController {
    @Autowired
    VendorService service;

    @GetMapping
    @Description("This method will be used to get all Vendors")
    @ApiOperation(value = "Get list of Vendors")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list")
        }
    )
    public ResponseEntity<List<Vendor>> getAllVendors() {
        List<Vendor> list = service.getAllVendors();

        return new ResponseEntity<List<Vendor>>(list, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @Description("This method will be used Vendor using vendor ID")
    @ApiOperation(value = "This method will be used Vendor using vendor ID")
    public ResponseEntity<Vendor> getVendorById(@PathVariable("id") String id)
            throws RecordNotFoundException {
        Vendor entity = service.getVendorById(id);

        return new ResponseEntity<Vendor>(entity, new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping
    @Description("This method will be used to create/update vendor")
    @ApiOperation(value = "This method will be used to create/update vendor")
    public ResponseEntity<Vendor> createOrUpdateVendor(Vendor vendor)
            throws RecordNotFoundException {
        Vendor updated = service.createOrUpdateVendor(vendor);
        return new ResponseEntity<Vendor>(updated, new HttpHeaders(), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @Description("This method will be used to delete vendor using vendor ID")
    @ApiOperation(value = "This method will be used to delete vendor using vendor ID")
    public HttpStatus deleteVendorById(@PathVariable("id") String id)
            throws RecordNotFoundException {
        service.deleteVendorById(id);
        return HttpStatus.OK;
    }

}