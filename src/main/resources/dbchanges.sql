CREATE TABLE vendor (
    id varchar(50) PRIMARY KEY,
    name VARCHAR(110) NOT NULL,
    address VARCHAR(250)
);

insert into vendor values("4a047eca-d610-11e9-bb65-2a2ae2dbcce4", "Robert Vadra" , "India");
insert into vendor values("4a0481c2-d610-11e9-bb65-2a2ae2dbcce4", "Peter Fleming" , "England");
insert into vendor values("4a048398-d610-11e9-bb65-2a2ae2dbcce4", "John Smith" , "Singapore");