package com.spring.service;

import com.spring.entity.Vendor;
import com.spring.entitymanager.VendorRepository;
import com.spring.exception.RecordNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class VendorService {

    @Autowired
    VendorRepository repository;
    @Autowired
    MessageSource messageSource;

    public List<Vendor> getAllVendors() {
        List<Vendor> vendorList = repository.findAll();

        if (vendorList.size() > 0) {
            return vendorList;
        } else {
            return new ArrayList<>();
        }
    }

    public Vendor getVendorById(String id) throws RecordNotFoundException {
        Optional<Vendor> vendor = repository.findById(id);

        if (vendor.isPresent()) {
            return vendor.get();
        } else {
            throw new RecordNotFoundException(messageSource.getMessage("com.spring.service.noVendorExistsForGivenId",null, LocaleContextHolder.getLocale()));
        }
    }

    public Vendor createOrUpdateVendor(Vendor entity) throws RecordNotFoundException {
        Optional<Vendor> vendor = repository.findById(entity.getId());

        if (vendor.isPresent()) {
            Vendor newEntity = vendor.get();
            newEntity.setId(entity.getId());
            newEntity.setName(entity.getName());
            newEntity.setAddress(entity.getAddress());

            newEntity = repository.save(newEntity);

            return newEntity;
        } else {
            entity = repository.save(entity);

            return entity;
        }
    }

    public void deleteVendorById(String id) throws RecordNotFoundException {
        Optional<Vendor> vendor = repository.findById(id);
        if (vendor.isPresent()) {
            repository.deleteById(id);
        } else {
            throw new RecordNotFoundException(messageSource.getMessage("com.spring.service.noVendorExistsForGivenId",null, LocaleContextHolder.getLocale()));
        }
    }
}